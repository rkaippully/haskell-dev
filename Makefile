all: 8.6.5

.PHONY: 8.6.5
8.6.5: 8.6/8.6.5/Dockerfile
	docker build -t rkaippully/haskell-dev:8.6.5 ./8.6/8.6.5
	docker tag rkaippully/haskell-dev:8.6.5 rkaippully/haskell-dev:8.6
	docker tag rkaippully/haskell-dev:8.6.5 rkaippully/haskell-dev:latest

.PHONY: push
push: 8.6.5
	docker push rkaippully/haskell-dev
