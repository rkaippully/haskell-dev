This repo contains Dockerfile sources for building a Haskell development/CI environment for Haskell.

## Software Included

- ghc
- stack
- cabal
- dhall-to-json and dhall-to-yaml
- stylish-haskell
- hlint
